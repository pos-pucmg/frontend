import http from "../http-common";

class ProcessoService {
   getAll() {
     return http.jsonprooducerhttp.get("/sigo/mgpi/processos");
   }
}

export default new ProcessoService();