import http from "../http-common";
import authHeader from './auth-header';

class NormaService {
  // getAll() {
  //   return http.jsonprooducerhttp.get("/sigo/mgn/normas");
  // }

  // get(id) {
  //   return http.jsonprooducerhttp.get(`/sigo/mgn/norma/${id}`);
  // }

  download(id) {
    return http.pdfproducerhttp.get(`/sigo/mgn/download/norma/${id}`, {headers: authHeader()});
  }

  inactivate(id) {
    return http.jsonprooducerhttp.put(`/sigo/mgn/inativar/${id}`, {headers: authHeader()});
  }

  // create(data) {
  //   return http.jsonprooducerhttp.post("/normas", data);
  // }

  // update(id, data) {
  //   return http.jsonprooducerhttp.put(`/sigo/mgn/norma/${id}`, data);
  // }

  // delete(id) {
  //   return http.delete(`/normas/${id}`);
  // }

  // deleteAll() {
  //   return http.delete(`/normas`);
  // }

  search(title) {
    return http.jsonprooducerhttp.get(`/sigo/mgn/normas?codigo=${title}`, {headers: authHeader()});
  }

  searchRemote(title) {
    return http.jsonprooducerhttp.get(`/sigo/mgn/externo/normas?codigo=${title}`, {headers: authHeader()});
  }

  add(norma) {
    return http.postjsonhttp.post("/sigo/mgn/importar", norma, {headers: authHeader()});
  }
}

export default new NormaService();