import axios from "axios";
//import authHeader from './services/auth-header';

const AWS_API_GATEWAY_URL = "https://4sxs15c1y1.execute-api.sa-east-1.amazonaws.com";

const jsonprooducerhttp = axios.create({
  //baseURL: "http://localhost:8080",
  baseURL: AWS_API_GATEWAY_URL,
  headers: {
    "Content-type": "application/json"
  }
//  headers: authHeader() 
});

const postjsonhttp = axios.create({
  //baseURL: "http://localhost:8080",
  baseURL: AWS_API_GATEWAY_URL,
  headers: {
    "Content-type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "*",
    "Access-Control-Allow-Headers": "*"
  }
});

const pdfproducerhttp = axios.create({
  //baseURL: "http://localhost:8080",
  baseURL: AWS_API_GATEWAY_URL,
  responseType: 'blob'
});

export default {jsonprooducerhttp,pdfproducerhttp,postjsonhttp};
